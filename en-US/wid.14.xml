<chapter id="watcher">
<title>Watcher</title>

<para>
  What neither Peter or Pink Sally had noticed was that when
  they&#39;d left the Liberty Hotel, they were being followed by
  Natalie Ferris-Brunet. She had no car of her own, so she was obliged
  to follow them on foot. Normally it would have been difficult or
  even impossible, but
  they stopped at least once every block for sightseeing. It made
  Natalie&#39;s job so easy that she suspected more than once that
  they not only knew that she was following but were deliberately
  waiting for her to catch them up.
</para>

<para>
  When they drove away from Madame Celeste&#39;s shoppe, Natalie had
  to go inside to find out where they&#39;d gone. She waved her
  insurance calling card around and mentioned that she&#39;d been
  scheduled to meet Mr. Asther there. Madame Celeste, of course, had
  confessed that they had left much sooner than expected but that they
  had mentioned dinner reservations at Schokolade, which one might
  have guessed anyway, it being famous as Mr. Asther&#39;s regular haunt.
</para>

<para>
  When Natalie arrived at Schokolade, she asked to be seated off in a
  corner, so her waiter took her to a table that was close to the
  kitchen. It was an ideal location, as she could see Peter and his
  companion, but she was out of their line of sight so that they would
  not see her unless they got up and wandered off into the kitchen.
</para>

<para>
  Natalie asked to use the telephone, and was directed to the courtesy
  phone nearby. She dug out Joseph Meyers&#39;s calling card and
  had the operator connect her.
</para>

<para>
  &#34;Detective Meyers,&#34; came an unfamiliar woman&#39;s voice.
</para>

<para>
  &#34;I&#39;m sorry,&#34; Natalie said, &#34;I was trying to contact
  Detective Joseph Meyers.&#34;
</para>

<para>
  &#34;Yes, this is his answering service. Have you a message for the
  detective?&#34;
</para>

<para>
  &#34;Yes, will you tell Detective Meyers that Miss Ferris-Brunet called.
  That&#39;s F-e-r-r-i-s dash B-r-u-n-e-t. I&#39;m at Schokolades, in the
  Strip District, and I should appreciate it if he could join me.&#34;
</para>

<para>
  &#34;Yes, I understand.&#34; the woman said.
</para>

<para>
  &#34;Could you see that he gets the message right away?&#34;
</para>

<para>
  &#34;I&#39;ll see if I can reach him,&#34; the woman said.
</para>

<para>
  Natalie went back to her table and tried to stall for time as much
  as she felt decently possible. Finally, she ordered an appetizer
  that cost as much as a full meal at any other restaurant.
</para>

<para>
  Detective Meyers showed up before the food did, and took a seat
  across from her and said, &#34;Imagine my surprise at your telephone
  message. I had no idea you were a gourmand. I&#39;ve never been here
  myself, but of course I know its reputation.&#34;
</para>

<para>
  &#34;I only just ordered an appetizer,&#34; Natalie said, &#34;and
  as a result I&#39;m sure I&#39;ve used up my mad money until next pay day.&#34;
</para>

<para>
  &#34;Making a meal of these breadsticks is sounding better and better.&#34;
</para>

<para>
  &#34;We could always ask our mutual friend to pick up the bill,&#34;
  Natalie said, and motioned in the direction of her oblivious dinner
  companions. 
</para>

<para>
  Joseph followed her gesture. &#34;Well, now I know why we&#39;re
  here. Pretty dame calls me and invites me to dinner, I figured there
  was an angle.&#34;	
</para>

<para>
  Natalie blushed involuntarily but tried to sound tough, &#34;There&#39;s
  <emphasis>always</emphasis> an angle.&#34;
</para>

<para>
  &#34;You said it, sister. Now what do you think <emphasis>their</emphasis> angle is?&#34;
</para>

<para>
  Natalie looked back at Peter and Pink Sally and considered what she
  saw. &#34;A friend visiting from out of town? A charity case? He
  took her out to buy dresses this afternoon.&#34;
</para>

<para>
  Natalie took out the picture that The Singer had given to her and
  pushed it toward Joseph. He looked at the photograph and back up at
  Pink Sally. &#34;It would help to have her last name, then at least
  we could check up on her. Unfortunately, the fact that they happen
  to live on the 19th floor does not give me the right to grill them
  for every personal detail I so please.&#34;
</para>

<para>
  &#34;Well, whomever she is, he&#39;s doing a good job of wining and
  dining her,&#34; Natalie said. 
</para>

<para>
  &#34;She&#39;s no friend and there&#39;s no such thing as
  charity. I don&#39;t know where he picked her up, or where she
  picked him up, but we are witnessing the time-honoured, traditional,
  proper Northern-Union-of-America courtship ritual.&#34;
</para>

<para>
  &#34;Lovely,&#34; Natalie said wryly. &#34;But the question is, why
  did I get called up to their floor, that night?&#34;
</para>

<para> 
  Joseph took a long moment to think things over. The waiter came
  by and delivered their paltry meal, which they shared with no
  intention of actually ordering an entr&#233;e. Once he got a little
  bit of food in him, he started up again&#58; &#34;Coincidence. The
  perp found the floor with the least amount of activity and camped out
  in the supply room for a day or two. Jeff the Survey Man was a
  temporary renter and probably blindsided the perp in that respect,
  because the floor is otherwise practically vacated. The only occupied
  apartment on that entire floor is Peter&#39;s, and Peter is gone for
  most of the day every day.&#34;
</para>

<para>
  &#34;So I wind up on the 19th floor because it&#39;s is the empty
  floor, only Jeff has moved in and intercepts me. So Death calls me
  on the courtesy telephone to get me back up to the 19th floor. That
  sounds fine, only there&#39;s no motive.&#34;
</para>

<para>
  &#34;Yours was, so far, a victimless crime, my dear&#34; Joseph
  said. &#34;The most we have on this Death fella so far is that he
  intimidated you. It&#39;s not uncommon in extortion plots. Scare
  the victim, make them jumpy, on edge. And then call them up late one
  night and ask for what they really want.&#34;
</para>

<para>
  &#34;I haven&#39;t a thing in the world,&#34; Natalie
  protested. &#34;I&#39;m an account manager at a local insurance
  agency, for which I get paid a whopping 35 dollars a week.&#34;
</para>

<para>
  &#34;Meaning you have access to the financial and personal records of people and
  businesses alike,&#34; Joseph said.
</para>

<para>
  Natalie saw her MULTICS terminal flash before her eyes and the truth
  of what Joseph was theorizing sank in.
</para>

<para>
  Joseph could tell he&#39;d hit his mark, so he kept going,
  &#34;It&#39;s not you that someone is interested in,
  Natalie. It&#39;s Peter Asther, or his girl A&#239;da, and they
  intend to get there through you.&#34;
</para>

<para>
  &#39;Gee whiz, I had no idea&#34; Natalie said weakly. &#34;Then you
  think he&#39;ll contact me again?&#34;
</para>

<para>
  Joseph picked up the photograph of the statue from the table and
  showed it to Natalie. She understood. He said, &#34;And he, or she?,
  will contact you again soon. Maybe with a request for
  information. Or maybe with a task. When this happens, you must tell
  me.&#34;
</para>

<para>
  &#34;I understand.&#34; Something compelled her, however, not to
  mention The Singer&#39;s purpose for giving her the photograph of
  Pink Sally.
</para>

<para>
  &#34;Come on, let&#39;s go meet my friend Ken,&#34; Joseph said,
  standing and putting on his coat.
</para>

<para>
  They left the restaurant and took a bus uptown. From there, the bus
  stop on Penn, they walked toward a quiet train yard, and then turned
  right into a little alley behind what looked to be warehouses and
  abandoned storefronts. She didn&#39;t know where exactly they were,
  but she didn&#39;t have to know that in order to recognise it as the
  part of town her mother had always warned her about. Natalie
  instinctively clung to Joseph&#39;s arm, and they continued down the
  alley to its midway point. Here, they stopped at a red building with
  a few broken windows and doors that were bruised and water-stained,
  and went inside.
</para>

<para>
  They ascended the rickety stairway to the fourth floor, and Joseph
  knocked upon the door to apartment 404<!-- file not found -->. There was an eerie silence
  suggesting that no one was home, but moments later the door opened.
</para>

<para>
  The occupant was a tired-looking man dressed in slacks and a
  t-shirt. He hadn&#39;t shaved recently and his hair was not combed.
  This was Ken, the Eagle.
</para>

<para>
  &#34;Detective Meyers,&#34; Ken said. &#34;Come on in.&#34;
</para>

<para>
  Natalie and Joseph entered the apartment and found it about as clean
  and organized as the building&#39;s exteriour. Ken was a pack-rat,
  like most Eagles were, and working alone day after day, he rarely
  felt the need to do much cleaning up.
</para>

<para>
  Ken preceded them into the apartment and cleared off the kitchen
  table enough so that they could sit at it.
</para>

<para>
  Natalie tugged on Joseph&#39;s sleeve, and when he looked at her,
  she didn&#39;t even need to voice her question.
</para>

<para>
  &#34;As a part of the joint effort in &#39;cross pollinating&#39; the
  political systems of the Third Reich and the Northern Union, America
  exported some simple ideas about voting for representation and
  the Third Reich exported the S.S.&#34;
</para>

<para>
  &#34;You mean there&#39;s an S.S. here in the Union?&#34; Natalie
  said.
</para>

<para>
  &#34;You&#39;re sitting in it,&#34; Ken said, overhearing
  them. &#34;Well, one small part of it, anyway.&#34;
</para>

<para>
  &#34;We don&#39;t call it the S.S. here. Eisenhower came up with the
  name. We call it &#39;the Eagles&#39;. Truth be told, it&#39;s the
  same organization. In fact, the director of the Eagles is an
  S.S. man, isn&#39;t he, Ken?&#34;
</para>

<para>
  &#34;Was,&#34; Ken said. &#34;He&#39;s an Eagle now. But he&#39;s a
  kraut through and through. Even lives over there. I think he has a summer home some
  place in Connecticut to make his citizenship official. He&#39;s our adviser and &#39;logistic
  coordinator&#39;. Whatever that means.&#34; 
</para>

<para>
  &#34;They watch people,&#34; Joseph said flatly to
  Natalie. &#34;Unlike the S.S., they have no authority to make
  arrests on Union soil, or even to contact the police. But if the
  police contact them, they also have no authority to withhold
  information.&#34;
</para>

<para>
  &#34;Coffee?&#34; Ken asked Natalie, pulling a chair out for her at
  the kitchen table. He cleared off the little stove and emptying out
  the coffee maker.
</para>

<para>
  &#34;No, thank you,&#34; Natalie said.
</para>

<para>
  &#34;What do you need, Joe?&#34; Ken asked, joining them at the
  table.
</para>

<para>
  &#34;Peter Asther, you know him?&#34;
</para>

<para>
  &#34;Sure,&#34; Ken said, rifling through a binder the size of the
  New York City phone directory. &#34;Owner of the Asther
  Corporation. Rich fellow. What about him?&#34;
</para>

<para>
  &#34;We want to know what he&#39;s up to.&#34;
</para>

<para>
  Ken took a few moments to review his notes. &#34;We watched him a
  few years back. He was wholly uninteresting. We recently got back to
  him. He&#39;s still wholly uninteresting.&#34;
</para>

<para>
  &#34;Nothing unusual about his activities at all?&#34; Joseph said.
</para>

<para>
  &#34;The guy&#39;s a model citizen. He goes to work, he goes home,
  he listens to opera, he goes to bed. Rinse and repeat. The reason we
  keep going back to him at all is because of that minor
  distinguishing characteristic he has.&#34;
</para>

<para>
  Joseph and Natalie both nodded and exchanged glances. Joseph voiced
  what they were both thinking, &#34;He&#39;s coloured.&#34;
</para>

<para>
  &#34;No,&#34; Ken said, &#34;who cares about that? he&#39;s
  rich. What interests us is that he owns about half of the
  city. Literally. That puts him in an interesting position. But
  politically speaking, he seems to be mostly conservative and
  interested in nothing else but money. So, uninteresting.&#34;
</para>

<para>
  Joseph motioned to Natalie and she handed over the photograph of
  the statue. Joseph showed it to Ken, who just shook his
  head. &#34;What is it?&#34;
</para>

<para>
  &#34;It&#39;s a statue,&#34; Joseph said.
</para>

<para>
  &#34;I see <emphasis>that</emphasis>,&#34; Ken said. &#34;WHy are
  you showing it to me?&#34;
</para>

<para>
  &#34;We think that Peter had it in his apartment. Know anything about it?&#34;
</para>

<para>
  &#34;Oh sure, I remember that,&#34; Ken said, nodding. &#34;He had
  it commissioned about four years ago.&#34; 
</para>

<para> 
  Natalie said, &#34;What about very recently? Haven&#39;t you
  seen anything suspicious in the past month? not from Peter himself,
  but the people around him. Maybe other people on his apartment floor
  at the Liberty Hotel?&#34;
</para>

<para>
  &#34;Sorry, lady, we gave up on watching Peter when he
  wouldn&#39;t so much as cheat on his income taxes. This fella ought to get
  out of the construction and real estate racket and sell soap
  door-to-door, because he&#39;s just that clean.&#34; 
</para>
</chapter>