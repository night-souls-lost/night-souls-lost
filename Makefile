# Makefile

html: ./en-US/book.xml
	xmlto html ./en-US/book.xml -o ./html

pdf: ./en-US/book.xml
	xmlto fo ./en-US/book.xml -x ./lulu.xsl \
	--stringparam page.width=6in \
	--stringparam page.height=9in \
	--stringparam body.font.family="Times" \
	--stringparam body.font.master=10 \
	--stringparam body.font.size=10 \
	--stringparam page.margin.inner=0.5in \
	--stringparam page.margin.outer=1in \
	--stringparam page.margin.top=0.75in \
	--stringparam page.margin.bottom=0.75in \
	--stringparam chapter.autolabel=1 \
	-o pdf
	fop pdf/book.fo pdf/book.pdf
	gs -sFONTPATH=../fonts \
	-o pdf/book+fonts.pdf \
	-sDEVICE=pdfwrite \
	-dPDFSETTINGS=/prepress \
	pdf/book.pdf
	-rm -f pdf/book.fo
	mv pdf/book+fonts.pdf pdf/theNightThatSoulsWereLost_2ed.pdf

ebook:	pdf/theNightThatSoulsWereLost_2ed.pdf
	pdftk ./images/coverart.pdf \
	pdf/theNightThatSoulsWereLost_2ed.pdf \
	./images/backart.pdf cat output \
	pdf/theNightThatSoulsWereLost_2ed+covers.pdf
	-rm -f pdf/book.fo

epub:	en-US/book.xml
	xmlto epub \
	-x ./docbook-xsl-1.77.1/epub/docbook.xsl \
	en-US/book.xml
	mv book.epub theNightThatSoulsWereLost_2ed.epub
 
tidy:
	-rm -f ./pdf/*.fo

clean:
	-rm -f tmp.xml
	-rm -f *.header
	-rm -f html/*.html
	-rm -rf ./pdf/
